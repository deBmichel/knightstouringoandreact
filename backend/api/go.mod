module knightstouringoandreact/backend/api

// +heroku goVersion go1.16
go 1.16

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/gorilla/websocket v1.4.2
	github.com/joho/godotenv v1.3.0
)
