package db

import (
	"log"
	"sync"
)

// DB is the datastructure to emulate a db.
type DB struct {
	mu        sync.Mutex
	registers []*register
	gncounter *counter
}

// SaveNewSolutionOfKnightTour save a new solution of a KnightTour in
// the datastructure used as db returning the id of the saved solution.
func (instance *DB) SaveNewSolutionOfKnightTour(board *[64]int) int {
	instance.mu.Lock()
	defer instance.mu.Unlock()

	id := len(instance.registers)
	register := createRegister(
		&iDRegisterKnightTour{ID: id},
		&answerRegisterKnightTour{Answer: board})
	instance.registers = append(instance.registers, register)

	return id
}

// UpdateTourCounter undate the counter of solved tours, increment in one.
func (instance *DB) UpdateTourCounter() {
	instance.mu.Lock()
	defer instance.mu.Unlock()

	instance.gncounter.SolvedTours++
}

// GetTourCounter returns the counter of solved tours.
func (instance *DB) GetTourCounter() int64 {
	instance.mu.Lock()
	defer instance.mu.Unlock()

	return instance.gncounter.SolvedTours
}

// GetTourUsingID returns a solved tour using the Id of the Tour.
func (instance *DB) GetTourUsingID(id int) *[64]int {
	for i := 0; i < len(instance.registers); i++ {
		if instance.registers[i].ID.ID == id {
			return instance.registers[i].Answer.Answer
		}
	}

	return nil
}

var (
	// ErrForcedSingletonPattern The unique global var to force singleton pattern.
	ErrForcedSingletonPattern *DB

	// ErrPttrnImsingletononcer The unique sync.Once to create unique instance of db.
	ErrPttrnImsingletononcer sync.Once
)

// Ins creates one instance of DB one time. The idea is use singleton tactic to have
// only one instance of the datastructure db.
// return the unique instance of DB.
func Ins() *DB {
	ErrPttrnImsingletononcer.Do(func() {
		log.Println("Creating one instance of db threat safe..")
		ErrForcedSingletonPattern = new(DB)
		ErrForcedSingletonPattern.gncounter = new(counter)
	})

	return ErrForcedSingletonPattern
}
