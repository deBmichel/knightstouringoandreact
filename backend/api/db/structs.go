package db

type iDRegisterKnightTour struct {
	ID int
}

type answerRegisterKnightTour struct {
	Answer *[64]int
}

type register struct {
	ID     *iDRegisterKnightTour
	Answer *answerRegisterKnightTour
}

type counter struct {
	SolvedTours int64
}

func createRegister(id *iDRegisterKnightTour, ans *answerRegisterKnightTour) *register {
	return &register{
		ID:     id,
		Answer: ans,
	}
}
