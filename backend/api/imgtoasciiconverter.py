import sys
import pywhatkit as conv

source_path = sys.argv[1]
target_path = source_path + 'toascii.text'

print ("using", source_path, target_path)

conv.image_to_ascii_art(source_path, target_path)
