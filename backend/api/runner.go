package main

import (
	"knightstouringoandreact/backend/api/knighttour"
	"log"
	"os"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	_ "github.com/joho/godotenv/autoload"
)

const (
	timelive = 12
)

func GetValidOrigin() string {
	return os.Getenv("ALLOWEDORIGIN")
}

func config() cors.Config {
	return cors.Config{
		AllowOrigins: []string{
			// It is interesting , because linters add problems but the explanation:
			// https://github.com/gin-contrib/cors/blob/64faa2b8f983fa76a2353004de59fd8a9a1f65c7/cors.go#L21
			// is clear.
			GetValidOrigin(),
		},
		AllowMethods: []string{
			"POST", "GET", "OPTIONS", "DELETE", "PATCH",
		},
		ExposeHeaders: []string{
			"Content-Length", "Content-Type", "Access-Control-Allow-Methods",
		},
		AllowHeaders: []string{
			"Accept", "Access-Control-Allow-Origin", "Accept-Encoding", "Authorization",
			"Content-Type", "Content-Length", "Host", "Origin", "Referrer", "User-Agent",
		},
		AllowOriginFunc: func(origin string) bool {
			// Review this: https://github.com/gin-contrib/cors/blob/64faa2b8f983fa76a2353004de59fd8a9a1f65c7/cors.go#L21
			return origin == GetValidOrigin()
		},
		MaxAge:                 timelive * time.Hour,
		AllowAllOrigins:        false,
		AllowWildcard:          false,
		AllowBrowserExtensions: false,
		// Here I found a logic error ... My router should block web socket
		// But I am using websockets ,,, It Is really interesting.
		// Was interesting to know than gin allow websockets in local but in production the
		// flag should be true .... Amazing ...
		AllowWebSockets:  true,
		AllowFiles:       false,
		AllowCredentials: true,
	}
}

func main() {
	mainRouter := gin.Default()

	mainRouter.Use(cors.New(config()))
	knighttour.WSKnighttour(mainRouter)

	if err := mainRouter.Run(); err != nil {
		log.Println("Error on mainRouter", err)
		panic(err)
	}
}
