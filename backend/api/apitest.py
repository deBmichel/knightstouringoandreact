import asyncio
import aiohttp

async def post(url=None, data=None):
	async with aiohttp.ClientSession() as session:
		async with session.post(url, json=data) as response:
			return await response.content.read()

async def solve_tour_with_coordinates(x, y):
	url = f'http://{"knightstourbackend.herokuapp.com"}/knighttra/knightstour/solveatour'
	data={ "x": x, 'y': y}
	return await post(url=url, data=data)
	

loop = asyncio.get_event_loop()
tasks = [
	asyncio.ensure_future(solve_tour_with_coordinates(2, 4)),
	#asyncio.ensure_future(solve_tour_with_coordinates(0, 0)),
	asyncio.ensure_future(solve_tour_with_coordinates(7, 7)),
	#asyncio.ensure_future(solve_tour_with_coordinates(1, 3)),
	#asyncio.ensure_future(solve_tour_with_coordinates(5, 5)),
	#asyncio.ensure_future(solve_tour_with_coordinates(7, 6)),
	#asyncio.ensure_future(solve_tour_with_coordinates(9, 10)),
	#asyncio.ensure_future(solve_tour_with_coordinates(-1, 0)),
]
loop.run_until_complete(asyncio.wait(tasks))
for task in tasks:
	print( task.result() )


