package knighttra

import (
	"crypto/rand"
	"errors"
	prntr "fmt"
	"log"
	"math/big"
)

const (
	// Board width and height, is a public const and ensure 8x8 board.
	BoardWaH = 8
)

var (
	// ErrInvalidCoordenates is a custom error to invalid coordinates in the algorithm (out of 8x8 board).
	ErrInvalidCoordenates = errors.New("invalid Coordinates to solve the algorithm, not in 8x8 limits board")

	// ErrKnightTourNotFound is a custom error in case of a KnightTourNotFound.
	ErrKnightTourNotFound = errors.New("KnightTourNotFound")
)

// cx returns move pattern on basis of the change of x coordinates.
func cx() [BoardWaH]int {
	return [BoardWaH]int{1, 1, 2, 2, -1, -1, -2, -2}
}

// cy returns move pattern on basis of the change of y coordinates.
func cy() [BoardWaH]int {
	return [BoardWaH]int{2, -2, 1, -1, 2, -2, 1, -1}
}

// getRandomNumberInRange return a random int number between cero and maxval-1.
func getRandomNumberInRange(maxval int) int {
	themax := maxval
	if themax == 0 {
		themax = 1
	}

	numbergen, err := rand.Int(rand.Reader, big.NewInt(int64(themax)))
	if err != nil {
		log.Println("Critical error on getRandomNumberInRange", err.Error())
		panic(err)
	}

	return int(numbergen.Int64())
}

// isinthelimits restricts the knight to remain within the 8x8 chessboard.
func isinthelimits(x, y int) bool {
	return ((x >= 0 && y >= 0) && (x < BoardWaH && y < BoardWaH))
}

// isempty checks whether a square is valid and empty or not.
func isempty(board *[64]int, x, y int) bool {
	return (isinthelimits(x, y)) && (board[y*BoardWaH+x] < 0)
}

// getDegree returns the number of empty squares adjacent to (x, y).
func getDegree(board *[64]int, x, y int) int {
	count := int(0)

	for i := 0; i < BoardWaH; i++ {
		if isempty(board, (x + cx()[i]), (y + cy()[i])) {
			count++
		}
	}

	return count
}

// nextMove picks next point using Warnsdorff's heuristic.
// returns false if it is not possible to pick next point.
func nextMove(board *[64]int, x, y *int) bool {
	minDegIdx, c, minDeg, nx, ny := -1, 0, (BoardWaH + 1), 0, 0
	// Try all N adjacent of (*x, *y) starting from a random adjacent.
	// find the adjacent with minimum degree.
	start := getRandomNumberInRange(BoardWaH)

	for count := 0; count < BoardWaH; count++ {
		i := (start + count) % BoardWaH
		nx = *x + cx()[i]
		ny = *y + cy()[i]
		c = getDegree(board, nx, ny)

		if (isempty(board, nx, ny)) && c < minDeg {
			minDegIdx, minDeg = i, c
		}
	}
	// if we could not find a next cell
	if minDegIdx == -1 {
		return false
	}
	// store coordinates of next point
	nx, ny = *x+cx()[minDegIdx], *y+cy()[minDegIdx]
	// mark next move
	board[ny*BoardWaH+nx] = board[(*y)*BoardWaH+(*x)] + 1
	// update next point
	*x, *y = nx, ny

	return true
}

// neighbour checks its neighbouring squares. If the knight ends on a square
// that is one knight's move from the beginning square, then tour is closed.
func neighbour(x, y, xx, yy int) bool {
	for i := 0; i < BoardWaH; i++ {
		if ((x + cx()[i]) == xx) && ((y + cy()[i]) == yy) {
			return true
		}
	}

	return false
}

// findClosedTour generates the legal moves using warnsdorff's heuristics.
// returns false if not possible.
func findClosedTour(sx, sy int) (bool, *[64]int) {
	// Filling up the chessboard matrix with -1's
	board := [BoardWaH * BoardWaH]int{}
	for i := 0; i < BoardWaH*BoardWaH; i++ {
		board[i] = -1
	}
	// Current points are same as initial points
	x, y := sx, sy
	// Mark first move.
	board[y*BoardWaH+x] = 1
	// Keep picking next points using Warnsdorff's heuristic
	for i := 0; i < BoardWaH*BoardWaH-1; i++ {
		if !nextMove(&board, &x, &y) {
			return false, &board
		}
	}
	// Check if tour is closed (Can end at starting point)
	if !neighbour(x, y, sx, sy) {
		return false, &board
	}

	return true, &board
}

// FindknightsTour is the driver function of the algorithm. It is a composition of funcs.
// returns a solved board *[64]int.
func FindknightsTour(x, y int) (*[64]int, error) {
	if isinthelimits(x, y) {
		tries := 1
		found, board := findClosedTour(x, y)

		for !found {
			tries++

			found, board = findClosedTour(x, y)
		}

		prntr.Println("Tries in to get a solution in coordinate", x, y, "=", tries)

		return board, nil
	}

	return nil, ErrInvalidCoordenates
}

// Formatsolvedboard convert the board to a human readable format
// this formater is only visible in the backend and really the execution
// can be faster using one for and discrete maths but here the idea is
// go slow.
func Formatsolvedboard(board *[64]int) string {
	formatedboard := ""

	for i := 0; i < BoardWaH; i++ {
		for j := 0; j < BoardWaH; j++ {
			formatedboard += prntr.Sprintf("%d\t", board[j*BoardWaH+i])
		}

		formatedboard += "\n"
	}

	prntr.Print(formatedboard)

	return formatedboard
}
