// C++ program to for Kinight's tour problem using Warnsdorff's algorithm 
// source: https://www.geeksforgeeks.org/warnsdorffs-algorithm-knights-tour-problem/
// It is a modified version to understand and change the original 100% random focus.
// you can compile vía command :
// g++ warnsdorffs-algorithm-mod.c++
// and execute vía command:
// ./a.out < input.ktp
// It is funny to :)
// @Author: Michel MS


#include <iostream>

using namespace std;

// Move pattern on basis of the change of 
// x coordinates and y coordinates respectively 
#define BoardWaH 8 
static int cx[BoardWaH] = {1,1,2,2,-1,-1,-2,-2}; 
static int cy[BoardWaH] = {2,-2,1,-1,2,-2,1,-1}; 
  
// function restricts the knight to remain within 
// the 8x8 chessboard 
bool limits(int x, int y) 
{ 
    return ((x >= 0 && y >= 0) && (x < BoardWaH && y < BoardWaH)); 
} 
  
/* Checks whether a square is valid and empty or not */
bool isempty(int board[], int x, int y) 
{ 
    return (limits(x, y)) && (board[y*BoardWaH+x] < 0); 
} 
  
/* Returns the number of empty squares adjacent 
   to (x, y) */
int getDegree(int board[], int x, int y) 
{ 
    int count = 0; 
    for (int i = 0; i < BoardWaH; ++i) 
        if (isempty(board, (x + cx[i]), (y + cy[i]))) 
            count++; 
  
    return count; 
} 
  
// Picks next point using Warnsdorff's heuristic. 
// Returns false if it is not possible to pick 
// next point. 
bool nextMove(int board[], int *x, int *y) 
{ 
    int min_deg_idx = -1, c, min_deg = (BoardWaH+1), nx, ny; 
  
    // Try all N adjacent of (*x, *y) starting 
    // from a random adjacent. Find the adjacent 
    // with minimum degree. 
    int start = rand()%BoardWaH; 
    for (int count = 0; count < BoardWaH; ++count) 
    { 
        int i = (start + count)%BoardWaH; 
        nx = *x + cx[i]; 
        ny = *y + cy[i]; 
        if ((isempty(board, nx, ny)) && 
           (c = getDegree(board, nx, ny)) < min_deg) 
        { 
            min_deg_idx = i; 
            min_deg = c; 
        } 
    } 
  
    // IF we could not find a next cell 
    if (min_deg_idx == -1) 
        return false; 
  
    // Store coordinates of next point 
    nx = *x + cx[min_deg_idx]; 
    ny = *y + cy[min_deg_idx]; 
  
    // Mark next move 
    board[ny*BoardWaH + nx] = board[(*y)*BoardWaH + (*x)]+1; 
  
    // Update next point 
    *x = nx; 
    *y = ny; 
  
    return true; 
} 
  
/* displays the chessboard with all the 
  legal knight's moves */
void print(int board[]) 
{ 
    for (int i = 0; i < BoardWaH; ++i) 
    { 
        for (int j = 0; j < BoardWaH; ++j) 
            printf("%d\t",board[j*BoardWaH+i]); 
        printf("\n"); 
    } 
} 
  
/* checks its neighbouring sqaures */
/* If the knight ends on a square that is one 
   knight's move from the beginning square, 
   then tour is closed */
bool neighbour(int x, int y, int xx, int yy) 
{ 
    for (int i = 0; i < BoardWaH; ++i) 
        if (((x+cx[i]) == xx)&&((y + cy[i]) == yy)) 
            return true; 
  
    return false; 
} 
  
/* Generates the legal moves using warnsdorff's 
  heuristics. Returns false if not possible */
bool findClosedTour(int sx, int sy) 
{ 
    // Filling up the chessboard matrix with -1's 
    int board[BoardWaH*BoardWaH]; 
    for (int i = 0; i< BoardWaH*BoardWaH; ++i) 
        board[i] = -1; 
    
    // Current points are same as initial points 
    int x = sx, y = sy; 
    board[y*BoardWaH+x] = 1; // Mark first move. 
  
    // Keep picking next points using 
    // Warnsdorff's heuristic 
    for (int i = 0; i < BoardWaH*BoardWaH-1; ++i) 
        if (nextMove(board, &x, &y) == 0) 
            return false; 
  
    // Check if tour is closed (Can end 
    // at starting point) 
    if (!neighbour(x, y, sx, sy)) 
        return false; 
  
    print(board); 
    return true; 
} 
  
// Driver code 
int main() 
{ 
    int sx, sy;

    cin >> sx;
    cin >> sy;

    // While we don't get a solution 
    while (!findClosedTour(sx, sy)) 
    { 
    ; 
    } 
    
  return 0; 
} 
