/* the knighttra_test module contains tests to solve the knights tour
problem using the knights tour algorithm translated by Michel MS.

execute test vía fast commands :
	go test -bench=BenchmarkTesterKnighttra -benchtime=20s
    go test

@Author: Michel MS.*/
package knighttra_test

import (
	prntrfmt "fmt"
	"log"
	"reflect"
	"testing"

	knighttra "knightstouringoandreact/backend/api/knighttra"
)

func TestFactory(t *testing.T) {
	t.Parallel()
	log.Println("Executing knighttra test with valid coordinates, review Code and behavior")
	prntrfmt.Println("using test values:", 2, 3)

	_, err := knighttra.FindknightsTour(2, 3)
	if err != nil {
		panic(err)
	}

	log.Println("Finish knighttra test with valid coordinates, review Code and behavior")
	prntrfmt.Println()
}

func benchmarkExecuteKnighttra(b *testing.B) {
	// Linting review found test helper function should start from b.Helper() (thelper)
	// I had to review this source https://github.com/kulti/thelper and learn about:
	// With this call go test prints correct lines of code for failed tests. Otherwise,
	// printed lines will be inside helpers functions.
	b.Helper()

	var (
		// Forcer is a minor try to avoid compiling optimization 3:)
		forcer error
		r      error
	)

	for i := 0; i < b.N; i++ {
		_, r = knighttra.FindknightsTour(3, 7)
	}

	forcer = r
	// A minor func to use the forcer and comply with the basic rules
	// of Golang: If you define a variable it is to use it
	reflect.TypeOf(forcer)
	prntrfmt.Println()
}

func BenchmarkTesterKnighttra(b *testing.B) {
	log.Println("Executing BenchmarkTesterKnighttra test with valid coordinates, review Code and behavior")
	benchmarkExecuteKnighttra(b)
	log.Println("Finish BenchmarkTesterKnighttra test with valid coordinates, review Code and behavior")
}
