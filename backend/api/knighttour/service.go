package knighttour

import (
	"github.com/gin-gonic/gin"
)

const (
	swsgroup   = "/knighttra/knightstour"
	cmmnerrrws = "/knighttra/knightstour failed, contact development knighttragr team ...."
)

// WSKnighttour define the service group /knighttra/knightstour to use in the main Router.
func WSKnighttour(route *gin.Engine) {
	cnddtsrgstrRouter := route.Group(swsgroup)
	cnddtsrgstrRouter.GET("/solvedtours", GetKnightSolvedTours)
	cnddtsrgstrRouter.GET("/claimtour:id", GetwebsocketClaimTour)
	cnddtsrgstrRouter.POST("/solveatour", PostKnightSolveATour)
}

/*
	Ideas to dinamyc endpoints ??
	func main() {
		router := gin.Default()
		router.GET("/test/:id/:name", testRouteHandler)
		router.Run(":8080")
	}

	func testRouteHandler(c *gin.Context) {
		id := c.Params.ByName("id")
		name := c.Params.ByName("name")
	}

	taken from:
	https://stackoverflow.com/questions/34046194/how-to-pass-arguments-to-router-handlers-in-golang-using-gin-web-framework
*/
