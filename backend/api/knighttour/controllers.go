package knighttour

import (
	"errors"
	"knightstouringoandreact/backend/api/knighttra"
	"log"
	"net/http"
	"regexp"
	"strconv"

	"github.com/gin-gonic/gin"
)

// GetKnightSolvedTours controls service /knighttra/knighttour/solvedtours.
func GetKnightSolvedTours(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"api":          "knight's tour by MichelMS",
		"Solved Tours": GetCount(),
	})
}

// PostKnightSolveATour controls service /knighttra/knighttour/solveatour.
func PostKnightSolveATour(c *gin.Context) {
	var thecor Coordinate

	if err := c.ShouldBindJSON(&thecor); err != nil {
		log.Println("Invalid json ? in PostKnightTourReccoordinate 1", err.Error())
		c.JSON(http.StatusUnprocessableEntity, gin.H{
			"Answer": "Problems with data in Coordinate: you are sending bad data",
		})

		return
	}

	ansboard, err := knighttra.FindknightsTour(*thecor.X, *thecor.Y)
	if err != nil {
		log.Println("Error on PostKnightTourReccoordinate in knighttra.FindknightsTour", err.Error())

		if errors.Is(err, knighttra.ErrInvalidCoordenates) {
			c.JSON(http.StatusOK, gin.H{
				"Answer": "Problems with data in Coordinate: you are sending bad data",
			})

			return
		}

		c.JSON(http.StatusUnprocessableEntity, cmmnerrrws)

		return
	}

	id := SaveSolvedTour(ansboard)

	UpdateCount()

	c.JSON(http.StatusOK, gin.H{
		"Answer":   "Tour Solved",
		"idAnswer": id,
		"answer":   knighttra.Formatsolvedboard(ansboard),
		"used X":   *thecor.X,
		"used Y":   *thecor.Y,
	})
}

// GetwebsocketClaimTour controls service /claimtour:id.
func GetwebsocketClaimTour(c *gin.Context) {
	id := c.Params.ByName("id")

	isnumber, err := regexp.MatchString(`^[0-9]+$`, id)
	if err != nil {
		log.Println("Error using regexp in received id", id)
		c.JSON(http.StatusUnprocessableEntity, gin.H{
			"Answer": "Tour Id was not received contact development.",
		})

		return
	}

	if isnumber {
		// Why not only use this ? : Simple I wanted to play with regexp :).
		idnumber, err := strconv.Atoi(id)
		if err != nil {
			log.Println("Failing in strconv.Atoi(id)", id)
			c.JSON(http.StatusUnprocessableEntity, gin.H{
				"Answer": "Api is having conversion problems contact development.",
			})

			return
		}

		tour := GetSolvedTour(idnumber)
		if tour != nil {
			wsckclaimtourmanager(c, tour)
		} else {
			c.JSON(http.StatusOK, gin.H{
				"Answer": "Tour id is not available to claim",
				"id":     id,
			})
		}
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{
			"Answer": "Tour Id is invalid",
		})
	}
}
