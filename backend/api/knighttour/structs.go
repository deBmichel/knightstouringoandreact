package knighttour

// Coordinate define the struct to control the json input to a
// valid coordinate in the board in the service group Coordinate.
type Coordinate struct {
	X *int `json:"x" binding:"required"`
	Y *int `json:"y" binding:"required"`
}
