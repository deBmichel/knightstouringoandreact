package knighttour

import (
	db "knightstouringoandreact/backend/api/db"
	"knightstouringoandreact/backend/api/knighttra"
	"log"
	"time"
)

const (
	empthystep = -1
)

// SaveSolvedTour save the solution of a solved tour.
func SaveSolvedTour(board *[64]int) int {
	return db.Ins().SaveNewSolutionOfKnightTour(board)
}

// UpdateCount update the total of all tours resolved by the api
// Exactly increases the number of tours resolved by one (1).
func UpdateCount() {
	db.Ins().UpdateTourCounter()
}

// GetCount returns the total of all tours resolved by the api.
func GetCount() int64 {
	return db.Ins().GetTourCounter()
}

// GetSolvedTour returns a solved tour or nil if id is not existent.
func GetSolvedTour(id int) *[64]int {
	return db.Ins().GetTourUsingID(id)
}

// Claimtour is a function to claim a solved tour, the objective was
// review channels and play with gorutines. Here I am using 3 for loops
// to do slow the execution; you can writte this claim algorithm using
// only one for loop and discrete maths to obtain the i, j coordinates
// used in the bidimensional frontend structure to represent the solved
// tour graphically.
func ClaimTour(tour *[64]int, tourstepi, tourstepj, move chan int) {
	awaitwstime, firstjump, limit := 500000000, 1, (knighttra.BoardWaH * knighttra.BoardWaH)
	for firstjump <= limit {
		for i := 0; i < knighttra.BoardWaH; i++ {
			for j := 0; j < knighttra.BoardWaH; j++ {
				if tour[j*knighttra.BoardWaH+i] == firstjump {
					move <- firstjump
					tourstepi <- i
					tourstepj <- j
					firstjump++

					time.Sleep(time.Nanosecond * time.Duration(awaitwstime))
				}
			}
		}
	}

	log.Println("firstjump:", firstjump, ", limit:", limit)

	// I know : It is not a great channels and gorutines implementation.

	close(tourstepi)
	close(tourstepj)
	close(move)
}
