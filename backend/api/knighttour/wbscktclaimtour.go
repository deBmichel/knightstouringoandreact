package knighttour

import (
	"errors"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

const (
	// Size of the buffer to write in websocket connection ? review the code.
	readwrittebuffersize = 25
	// Time to kill a web socket ? review the code.
	deadlinetimewsckt = 64
)

var (
	ErrWbsktUpgrade         = errors.New("WebsocketConnection has failed in Upgrade")
	ErrWbsktSetReadDeadline = errors.New("WebsocketConnection has failed in SetReadDeadline")
)

// This function is needed and vital to permit the correct management
// and function of a websocket of github.com/gorilla/websocket.
// The websocket connection can be any here we use connections created
// preparewebsocketconnection(clearly).
func validateOriginWebSocketRequest(req *http.Request) bool {
	// Is important review : The Origin Allowed is *
	return true
}

// wscktwritteFunc is a function to write a message using a websocket
// connection created with the method preparewebsocketconnection.
func wscktwritteFunc(mssg []byte, websckt *websocket.Conn) {
	if err := websckt.WriteMessage(1, mssg); err != nil {
		log.Println("Failing in writte process of the websocket knight's tour:", err)
		destroywebsocketconnection(websckt)
	}
}

// destroywebsocketconnection destroy a websocket connection created
// with the method preparewebsocketconnection.
func destroywebsocketconnection(conwebsckt *websocket.Conn) {
	if err := conwebsckt.Close(); err != nil {
		log.Println("Error closing a websocket connection the error was", err.Error())
	} else {
		log.Println("A websocket connection has been closed")
	}
}

// preparewebsocketconnection the name is clear and the code to.
// This function prepare a websocket connection using a gingonic
// context.
func preparewebsocketconnection(c *gin.Context) (*websocket.Conn, error) {
	wsupgrader := websocket.Upgrader{
		ReadBufferSize:  readwrittebuffersize,
		WriteBufferSize: readwrittebuffersize,
		CheckOrigin:     validateOriginWebSocketRequest,
	}

	conn, err := wsupgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		log.Println("Failed to set websocket upgrade:", err)
		c.JSON(http.StatusUnprocessableEntity, gin.H{
			"Answer": "Problem in wbskt upgrade contact development team ASAP",
		})

		return nil, ErrWbsktUpgrade
	}

	err = conn.SetReadDeadline(time.Now().Add(time.Second * time.Duration(deadlinetimewsckt)))
	if err != nil {
		log.Println("Failed to set SetReadDeadline upgrade:", err)
		c.JSON(http.StatusUnprocessableEntity, gin.H{
			"Answer": "Problem in wbskt deadlinetime contact development team ASAP",
		})

		return nil, ErrWbsktSetReadDeadline
	}

	return conn, nil
}

// wsckclaimtourmanager is the core function of this module.
// the objective was isolate the websocket code and have a
// specific module to manage websockets in the backend.
// this function is private and it is because this module is
// related with the Knight's tour claim process you dont have
// why use this in other services.
func wsckclaimtourmanager(c *gin.Context, tour *[64]int) {
	solverstepsknighttouri, solverstepsknighttourj, move := make(chan int), make(chan int), make(chan int)

	websocktcon, err := preparewebsocketconnection(c)
	if err != nil {
		log.Println("Fail preparing websocket connection", err.Error())

		return
	}

	go ClaimTour(tour, solverstepsknighttouri, solverstepsknighttourj, move)

	jumps := 0

	for {
		move, notfinishedm := <-move
		steptouri, notfinishedi := <-solverstepsknighttouri
		steptourj, notfinishedj := <-solverstepsknighttourj

		if !notfinishedi && !notfinishedj && !notfinishedm {
			wscktwritteFunc([]byte("<<< finished knight's tour Claim"), websocktcon)
			destroywebsocketconnection(websocktcon)

			break
		}

		if move != empthystep {
			coordinates := "Move : " + strconv.Itoa(move) + " ; ArrNotation (i:"
			coordinates += strconv.Itoa(steptouri) + ",j:" + strconv.Itoa(steptourj) + ")"
			mssg := []byte(coordinates)
			jumps++

			wscktwritteFunc(mssg, websocktcon)
		}
	}

	log.Println("Finished claim tour", jumps)
}

/*
	Is interesting see how the next lines produce the
	@panic :
		http: response.Write on hijacked connection from
		github.com/gin-gonic/gin.(*responseWriter).Write
		(response_writer.go:78)
        2021/03/07 07:07:02 [Recovery] 2021/03/07 - 07:07:02
        panic recovered: GET /knighttra/knightstour/claimtour1 HTTP/1.1

		http: connection has been hijacked

	@the lines:
	c.JSON(http.StatusOK, gin.H{
		"Answer": "Tour should be claimed in correct way",
	})

	From my point of view the problem is: Gorilla takes the c context and start writing
	and after I try writte gin detects c context connection has been taken (by gorilla)
	and this produces the error.
*/
