/*
	
	God have mercy on my soul for writing this module, this is a 
	really horrible module, here I was carried away by the desire 
	to see the board running and complete, but this is a real 
	disaster needs a good refactor.

	@Autor: Michel MS

*/

import { useEffect, useState, forwardRef, useImperativeHandle } from 'react';

import { H1AkTlvgl } from '../../common/styles/styles.js';
import { BoardStyle, Btns } from './styles.js';

// It is a fatal board and bad code I can make a refactor and clean this 
// serious disaster in all parts .....
/*
	It was the desire, now I know, it was the desire to see the backend 
	software run and the algorithm translated, I got carried away and 
	made a horrible code.
*/

import { 
	INITIAL_CLEAN_BOARD, INITIALID, BOARD_INACTIVE, BOARD_ACTIVE, GIVECLICKPLEASE, 
	BOARD_COMPLETED, TOUR_FINISHED, CLEAN_BOARD, WAITING_CLEAN_BOARD, LoadJsonRegister, 
	Handleclick, Historyindex, LogicBoard, PutAKnightJump, LogicBoardHIstory,  
	LogicBoardCopy, coordinatenumbers, coordinateletters, CleanBoardHistoryIndexHistory
} from './controller.js';

const Generateboardth = () => {
	return (<>
		<tr>
			<th></th>
			{coordinateletters.map((letter, index) => <th key={index}> {letter} </th>)}
		</tr>
	</>);
}

const Generateboardtrs = (props) => {
	let light = true;
	const trs = [];
	let key = 0;
	for (let i = 0; i < coordinatenumbers.length; i++) {
		const trsitms = [];
		trsitms.push(<th key={key}>{coordinatenumbers[i]}</th>);
		for (let j = 0; j < coordinatenumbers.length; j++) {
			key = key + 1;
			let colortype = light ? "light" : "dark";
			light = !light;
			let coordinate = LoadJsonRegister(i,j);
			trsitms.push(
				<td 
					key={key} className={colortype} 
					onClick={() => Handleclick(coordinate, props.requestscoordinator, props.requestcontroll)}>
					<div className="knight">{props.boardState[i][j]}</div>
				</td>
			);
		}
		light = !light;
		trs.push(trsitms);
	}

	return (<>{trs.map((item, index) => <tr key={index}>{item}</tr>)}</>);
}

const basebuttonhandler = (statetoreview, functiontobutton) => {
	if (statetoreview === "Board Active ...") {
		alert("a tour is running wait to the finish to play.");
	}

	if (statetoreview === "Board Inactive") {
		alert("Click a square on the board to play.");
	}

	if (statetoreview === "Board Completed") {
		functiontobutton();
	}
}


const LeftButton = (props) => {
	const leftstr = "<:-- ";
	return (<>
		<H1AkTlvgl onClick={() => props.handleLeftButton()}> {leftstr} </H1AkTlvgl>
	</>);
}

const OrgnButton = (props) => {
	const orgbstr = " {0} ";
	return (<>
		<H1AkTlvgl onClick={() => props.handleOrgnButton()}> {orgbstr} </H1AkTlvgl>
	</>);
}

const RightButton = (props) => {
	const rghtstr = " --:>";
	return (<>
		<H1AkTlvgl onClick={() => props.handleRightButton()}> {rghtstr} </H1AkTlvgl>
	</>);	
}

const CleanButton = (props) => {
	const orgbstr = " CleanBoard ";
	return (<>
		<H1AkTlvgl onClick={() => props.handleCleanButton()}> {orgbstr} </H1AkTlvgl>
	</>);
}

export const Board = forwardRef ((props, refcorecomponent) => {
	// I know about datastructures , and I think why not to use arrays to play with react ?
	const initialStateProps = [
		BOARD_INACTIVE,
		GIVECLICKPLEASE,
		INITIALID,
		LogicBoard,
		Historyindex,
		LogicBoardHIstory,
		INITIAL_CLEAN_BOARD,
	]

	const [componentPropsManager, setcomponentPropsManager] = useState(initialStateProps);

	const cleanBoard = () => {
		if (componentPropsManager[0] === BOARD_COMPLETED) {
			CleanBoardHistoryIndexHistory();
			props.tourStatusCleaner();
			// The war of the states  or am I crazy ????
			setcomponentPropsManager([
				BOARD_INACTIVE,
				GIVECLICKPLEASE,
				INITIALID,
				LogicBoard,
				Historyindex,
				LogicBoardHIstory,
				INITIAL_CLEAN_BOARD,
			]);
		}
		if (componentPropsManager[0] === BOARD_ACTIVE) {
			alert (" ... a Tour is running ...");
		}
		if (componentPropsManager[0] === BOARD_INACTIVE) {
			alert ("Click a square in the board");
		}
	}

	useImperativeHandle(refcorecomponent, 
		() => ({
			AddAjump(coordinates, newState, newId) {
				if (coordinates.length !== 1) {
					let changedboard = PutAKnightJump(coordinates[0],coordinates[1]);
					setcomponentPropsManager([
						BOARD_ACTIVE, 
						newState,
						newId,
						LogicBoard,
						Historyindex,
						LogicBoardHIstory,
						WAITING_CLEAN_BOARD
					]);
				} else {
					let statemodifier = componentPropsManager.slice();
					statemodifier[0] = BOARD_COMPLETED;
					statemodifier[1] = TOUR_FINISHED;
					statemodifier[6] = CLEAN_BOARD;
					setcomponentPropsManager(statemodifier);
				}
		    }
		}),
	);

	const requestcontroll = () => {
		if (componentPropsManager[0] === BOARD_COMPLETED) {
			return "You need to clean the board to run a new tour.";
		}

		if (componentPropsManager[0] === BOARD_ACTIVE) {
			return "The board is active please wait to finish the tour.";
		} 

		if (componentPropsManager[0] === BOARD_INACTIVE) {
			return true;
		}		
	}

	const handleLeftButton = () => {
		basebuttonhandler(componentPropsManager[0], () => {
			let statemodifier = componentPropsManager.slice();
			let step = statemodifier[4] - 1;
			if (step < 0) { step = (statemodifier[5].length) - 1; }
			statemodifier[4] = step;
			statemodifier[3] = statemodifier[5][statemodifier[4]].Historyindex;
			setcomponentPropsManager(statemodifier);
		});
	}
	const handleRightButton = () => {
		basebuttonhandler(componentPropsManager[0], () => {
			let statemodifier = componentPropsManager.slice();
			let step = statemodifier[4] + 1;
			if (step >= statemodifier[5].length) { step = 0; }
			statemodifier[4] = step;
			statemodifier[3] = statemodifier[5][statemodifier[4]].Historyindex;
			setcomponentPropsManager(statemodifier);
		});		
	}
	const handleOrgnButton = () => {
		basebuttonhandler(componentPropsManager[0], () => {
			let statemodifier = componentPropsManager.slice();
			statemodifier[3] = statemodifier[5][0].Historyindex;
			statemodifier[4] = 0;
			setcomponentPropsManager(statemodifier);
		});	
	}

	console.log("rendered Board");

	return (<>
		<BoardStyle>
			<H1AkTlvgl>{componentPropsManager[0]}</H1AkTlvgl>
			<H1AkTlvgl>{componentPropsManager[1]}</H1AkTlvgl>
			<H1AkTlvgl>{componentPropsManager[2]}</H1AkTlvgl>
			<table className="chess-board">
				<tbody>
					<Generateboardth />
					<Generateboardtrs 
						requestscoordinator={props.requestmanager}
						requestcontroll = {requestcontroll}
						boardState = {componentPropsManager[3]}
					/>
				</tbody>
			</table>

			<div className="btns">
				<span><LeftButton handleLeftButton = {handleLeftButton}/></span>
				<span><OrgnButton handleOrgnButton = {handleOrgnButton}/></span>
				<span><RightButton handleRightButton = {handleRightButton}/></span>
			</div>
			<H1AkTlvgl onClick = {() => {cleanBoard()}}>{componentPropsManager[6]}</H1AkTlvgl>
		</BoardStyle>
	</>);
});
