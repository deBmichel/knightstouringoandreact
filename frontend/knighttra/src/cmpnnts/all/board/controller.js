/*
	
	God have mercy on my soul for writing this module, this is a 
	really horrible module, here I was carried away by the desire 
	to see the board running and complete, but this is a real 
	disaster needs a good refactor.

	@Autor: Michel MS

*/

import * as ApiSolveTour from '../../../api/knightsreqsolvetour.js';

export const BOARD_INACTIVE = "Board Inactive";
export const BOARD_ACTIVE = "Board Active ...";
export const GIVECLICKPLEASE = "Click one square in the board:";
export const INITIALID = "";
export const INITIAL_CLEAN_BOARD = "<:--O--:>";
export const BOARD_COMPLETED = "Board Completed";
export const TOUR_FINISHED = "Tour finished";
export const CLEAN_BOARD = "Clean Board";
export const WAITING_CLEAN_BOARD = "..... tour is running.";


export const coordinateletters = ["a","b","c","d","e","f","g","h"];
export const coordinatenumbers = ["8","7","6","5","4","3","2","1"];
const generatelogicalBoard = (func) => {
	const trs = [];
	for (let i = 0; i < coordinatenumbers.length; i++) {
		const trsitms = [];
		for (let j = 0; j < coordinatenumbers.length; j++) {
			trsitms.push(func());
		}
		trs.push(trsitms);
	}
	return trs;
}

export var LogicBoard = generatelogicalBoard(()=>{return "";});
export const CleanLogicBoard = () => {
	LogicBoard = generatelogicalBoard(()=>{return "";});
}

export var LogicBoardHIstory = [];
export const CleanLogicBoardHIstory = () => {
	LogicBoardHIstory = [];
}

export var Historyindex = 0;
export const CleanHistoryindex = () => {
	Historyindex = 0;
}

export const PutAKnightJump = (i,j) => {
	LogicBoard[i][j] = "♞";
	// A beautifull lesson in js and copies of vars .... sure ....
	let actualstate = {Historyindex : JSON.parse(JSON.stringify(LogicBoard))};
	LogicBoardHIstory = LogicBoardHIstory.concat(actualstate);
	Historyindex++;
}

export const CleanBoardHistoryIndexHistory = () => {
	CleanLogicBoard();
	CleanLogicBoardHIstory();
	CleanHistoryindex();
}

export const LoadJsonRegister = (x, y) => { return JSON.stringify({ x: x, y: y }); }
export const Handleclick = (data, requestmanager, requestcontroll) => {
	let flaganswer = requestcontroll()
	console.log("flaganswer", flaganswer);
	if (flaganswer === true) {
		ApiSolveTour.RequestToSolveNewTour(data).then((knighttourans) => {
			if (knighttourans[0]) {
				requestmanager(knighttourans[1]);
			} 

			if (knighttourans[1] === ApiSolveTour.ERROR_BAD_COORDINATES_DATA) {
				alert(knighttourans[1] + " \n Did you modify the app in your machine ??");
			}

			/*if (knighttourans[1] === ERRORSYSTEM) {
				alert("We are having technical problems contact development ASAP \n" + knighttourans[1]);
			}*/
		});
	} else {
		alert(flaganswer);
	}
}

