/*
	MODULE: styles.js in board.
	
		Only the styles to apply to the board, here I dont have code logic.

		Is possible to create dinamyc styles but the logic to dinamyc styles is 
		more about if and props: NO LOOPS, no EXPORTABLE CONSTANTS, NO efects, NO 
		HOOKS,only styles, remember you have react and react components and 
		controllers to solve problems. YOU dont solve problems in the styles module.

	@Authors: Michel MS.
*/

import styled from '@emotion/styled';

import { H1AkTlvgl, fontAkayaTelivigala } from '../../common/styles/styles.js';

export const BoardStyle = styled.div`
	@font-face {
		font-family: 'Akaya Telivigala';
		src: URL(${fontAkayaTelivigala}) format('truetype');
	}
	
	font-family: 'Akaya Telivigala', cursive;
	text-align: center;
	align-items: center;

	.chess-board .light { background: #179e25; }
	.chess-board .dark { background: #000; }
	.chess-board { 
		border-collapse: collapse; 
		margin: 0 auto;
	}
	.chess-board th { 
		padding: .5em; 
		color: #00eb00;
	}
	.chess-board td {
		border: 1px solid; 
		width: 2em; 
		height: 2em; 
		color: #00eb00;
	}
	.btns span {
		display: inline-block;
		width: 100px;
	}

	.knight {
		color: #f3f0e1;
	}
`;
