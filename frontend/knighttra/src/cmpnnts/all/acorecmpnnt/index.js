/*
	MODULE: index.js in acorecomponent.

		I have some minor idea about software development and I thing that a
		core component is needed and should be clear, I think when you use the 
		default app.js as corecomponent you are not using the components idea
		and some new React Developers can have problems with the concept.

		Components are component. corecomponent is only one component : the main
		:).

		I think in this way. And code is short and if corecomponent should have
		a refactor process you really has a folder to add more components controllers
		and folders and maintaind a clear project structure, not create uggly code 
		in app.js .

	@Authors: Michel MS.
*/

import { useRef, useEffect } from 'react';

import { Header } from '../header/header.js';
import { Board } from '../board/board.js';
import { TourStatus } from '../tourstatus/tourstatus.js';

import { DivMainCmpnnt } from './styles.js';

const CoreComponent = (props) => {
	// Use closures is a beauty idea :) Taken from https://medium.com/a-layman/use-closure-to-store-variables-to-avoid-re-render-in-reactjs-4cedb3277f79
	// But finally I found this answer https://stackoverflow.com/questions/52018025/react-update-child-component-without-rerendering-parent
	// and reviewed this https://betterprogramming.pub/updating-state-from-properties-with-react-hooks-5d48693a4af8
	// about refs a good idea came from https://stackoverflow.com/questions/52591425/reactjs-typeerror-this-ref-current-method-is-not-a-function
	console.log("Created : Michel MS");
	const refcorecomponentstatus = useRef();
	const refcorecomponentboard = useRef();
	const isFirstMount = useRef(true);
	const solvingTour = useRef(false);
	const tourStatus = useRef("No tour is running ..");

	const IndicatefinishTour = (datehour) => { 
		solvingTour.current = false;
		console.log("finished tour at:", datehour);
	}

	const requestsolvetourmanager = (data) => {
		if (!solvingTour.current) {
			solvingTour.current = true;
			refcorecomponentstatus.current.updateStateAndId("Running a Tour", data);
		} else {
			alert("You are running a tour now please wait to finish.");
		}
	}

	const cleanStatus = () => {
		refcorecomponentstatus.current.updateStateAndId(tourStatus.current);
	}

	const listener = (coordinatesarray, status, idtour) => {
		//console.log("Claiming tour id:", coordinatesarray);
		refcorecomponentboard.current.AddAjump(coordinatesarray, status, idtour);
	}

	console.log("rendered CoreComponent ....");
	return (<>
		<Header/>
		<br />
		<DivMainCmpnnt>
			<div className="container" >
				<div>
					<Board 
						requestmanager= { requestsolvetourmanager }
						ref = { refcorecomponentboard }
						tourStatusCleaner = { cleanStatus }
					/>
				</div>
				<div>
					<TourStatus
						tourStatus = { tourStatus.current } requestfinisher = { IndicatefinishTour }
						tourId={ -1 } ref = { refcorecomponentstatus } listener = { listener }
					/>
				</div>
			</div>
		</DivMainCmpnnt>
	</>);
}

export default CoreComponent;