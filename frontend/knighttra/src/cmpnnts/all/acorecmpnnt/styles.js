/*
	MODULE: styles.js in acorecomponent.
	
		Only the styles to apply to the corecomponent, here I dont have code logic.

		Is possible to create dinamyc styles but the logic to dinamyc styles is 
		more about if and props: NO LOOPS, no EXPORTABLE CONSTANTS, NO efects, NO 
		HOOKS,only styles, remember you have react and react components and 
		controllers to solve problems. YOU dont solve problems in the styles module.

	@Authors: Michel MS.
*/

import styled from '@emotion/styled';

export const DivMainCmpnnt = styled.div`
	.container > div {
		display: flex;
		justify-content: center;
		text-align: center;
		color: black;
		border: 6px solid #00eb00;
	  	border-radius: 25px;
	}

	.container {
		display: grid;
		grid-gap: 1%;
		grid-template-columns: repeat(auto-fit, minmax(30%, 49%));
		grid-auto-rows: 100%;
		justify-content: center;
		text-align: center;
	}

`;
