/*
	MODULE: tourstatus.js

	The board needed a tourstatus component, you now to follow the tour 
	step by step, tourstatus.js is the component to show step by step the
	solution. Code is not complex and is almost clear, except by here we 
	are using jquery(review the websocket api module to have idea.);

	@Authors: Michel MS.
*/

import { useEffect, useState, useRef, forwardRef, useImperativeHandle } from 'react';
import $ from 'jquery'; 

import { ClaimTour } from './controller.js';
import { HeaderStatus } from './headerstatus.js';

import { DivScroll } from './styles.js';

export const TourStatus = forwardRef ((props, refcorecomponent) => {
	/*	
		Other lesson in js is very easy writte errated code, in js I should read and writte slow.

		Important lesson, I found a way to avoid rerendering of this component, and I love
		hooks they are pure beauty and avoid logic and long funcions: I had to read this:
		https://stackoverflow.com/questions/33613728/what-happens-when-using-this-setstate-multiple-times-in-react-component
		(But I have to be honest, use states is not a great idea , I preffer a dream in: Secuencial communication or charts)
	*/
	const refindextourstatus = useRef(); 
	const isInitialMount = useRef(true);
	const [componentPropsManager, setcomponentPropsManager] = useState([
		props.tourStatus, props.tourId
	]);

	const updateHeaders = (Header, SubHeader) => {
		refindextourstatus.current.updateHeaders(Header, SubHeader);
	}

	const finishrequesttour = (datehour) => {
		props.requestfinisher(datehour);
		updateHeaders("The tour really was finished ...", "Review the tour step by step");
	}
	
	useImperativeHandle(refcorecomponent, () => ({
		updateStateAndId(newState, newId) {
			setcomponentPropsManager([newState, newId]);
			// Interesting question : ¿How to avoid the re-render of the component HeaderStatus?
			// another internal ref ?? what about this line ?? 
			if (componentPropsManager[0] === "No tour is running ..") {
				// Hook abuse ... ??
				updateHeaders("Solving tour...", "Waiting finish");
			} else {
				updateHeaders("No tour is running ..", "No tour is running ..");
			}
	    }
	}));

	useEffect(() => {
		if (!isInitialMount.current) {
			if (componentPropsManager[0] !== "No tour is running ..") {
				ClaimTour(componentPropsManager[1], finishrequesttour, props.listener);
			} else {
				// Here I am cleaning the div when the board is cleaned
				// I am abusing the hooks ?
				$('#changestatus').empty();
			}
		} else {
			isInitialMount.current = false;
		}
		console.log("rendered TourStatus");
	});

	return (<>
		<DivScroll>
			<HeaderStatus 
				ref = {refindextourstatus} 
				header = { componentPropsManager[0] }
				subHeader = { componentPropsManager[0] }
			/>
			<div className="scroll" id="changestatus">
			</div>
			<br />
			<br />
		</DivScroll>
	</>);
});
