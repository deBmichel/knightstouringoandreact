/*
	MODULE: controller.js in tourstatus.
		
		I think a module to call only one clear and concrete function
		is a great idea, remember controller is controller.

		But here I think I should have two controllers: controllerheaderstatus.js
		and controllertourstatus, is obvious, code and order should be clear.
		
	@Authors: Michel MS.
*/


import { WebSocketHandlerToClaimSolvedTour } from '../../../api/knightsreqclaimtour.js';

export const ClaimTour = (tourid, finisher, websocketlistener) => {
	// I solved with a if but here the really important question is :
	// Why call twice this function ??
	console.log("Try ClaimTour using tourid:", tourid);
	if (tourid !== -1) {
		WebSocketHandlerToClaimSolvedTour(tourid, finisher, websocketlistener);
	}	
}
