/*	
	MODULE: headerstatus.js in tourstatus.

	The component tourstatus has a header(headerstatus), it is.
	headerstatus is called in tourstatus.

	Code is not complex, but here the question is :

		Is good code to React developers.

	@Authors: Michel MS.
*/

import { useState, forwardRef, useImperativeHandle } from 'react';

import { H1AkTlvgl } from '../../common/styles/styles.js';
import { DivScroll } from './styles.js';

export const HeaderStatus = forwardRef ((props, refindextourstatus) => {
	/*
		Important lesson, the rule : break the problem in minor parts apply in
		the frontend to solve great and minor challenges.
	*/
	const [stateprops, setstateprops] = useState([
		props.header, props.subHeader
	]);

	useImperativeHandle(refindextourstatus, () => ({
		updateHeaders(header, subHeader) { setstateprops([header, subHeader]);}
	}));

	console.log("rendered HeaderTourStatus");
	return (<>
		<DivScroll>
			<div>
				<H1AkTlvgl>{stateprops[0]}</H1AkTlvgl>
				{stateprops[1]}
				<br />
				<br />
			</div>
		</DivScroll>
	</>);
});

