/*
	MODULE: styles.js in tourstatus.
	
		Only the styles to apply to the tourstatus and to the headerstatus
		, here I dont have code logic. Here the initial situation was : Styles
		to header and tourstatus are the same, and we have only one module but
		really I should have TWO modules named stylestourstatus.js and 
		stylesheaderstatus.js, code and modules should be clear.

		Remeber :

		We developers write our own hell or paradise in a project, it is up to 
		us to write the ideas that make up a technological solution in a way 
		that all developers (old and new) can understand, maintain and be happy 
		at work, remember : it is we ourselves who make our work a hell or a 
		paradise.

		Is possible to create dinamyc styles but the logic to dinamyc styles is 
		more about if and props: NO LOOPS, no EXPORTABLE CONSTANTS, NO efects, NO 
		HOOKS,only styles, remember you have react and react components and 
		controllers to solve problems. YOU dont solve problems in the styles module.

	@Authors: Michel MS.
*/


import styled from '@emotion/styled';

import { fontAkayaTelivigala } from '../../common/styles/styles.js';

export const DivScroll = styled.div`
	@font-face {
		font-family: 'Akaya Telivigala';
		src: URL(${fontAkayaTelivigala}) format('truetype');
	}

	.scroll { 
		margin: 0 auto; 
		padding:4px; 
		background-color: black; 
		width: 70%; 
		height: 500px; 
		overflow-x: hidden; 
		overflow-y: auto; 
		text-align:justify; 
	}

	font-family: 'Akaya Telivigala', cursive;
	color: #00eb00;
`;
