/*
	MODULE: controller.js in header.
		
		It is the controller of the header, a clean and simple controller
		with only two exports and one function to call in header.js
		
	@Authors: Michel MS.
*/


import ImageHeaderInit from '../../../rsrcs/media/imgs/knightsTourAnimation.gif';
import ImageHeaderLogo from '../../../rsrcs/media/imgs/logo.png';

const changeHeaderImg = () => {
	let theImagePath = document.evaluate('//img[@id="ktmi"]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
	setTimeout(() => { theImagePath.setAttribute("src", ImageHeaderInit); }, 500);
	setTimeout(() => { theImagePath.setAttribute("src", ImageHeaderLogo); }, 8500);
}

export {ImageHeaderLogo, changeHeaderImg };
