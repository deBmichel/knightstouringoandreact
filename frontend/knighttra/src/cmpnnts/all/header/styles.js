/*
	MODULE: styles.js in header.
	
		Only the styles to apply to the header, here I dont have code logic.

		Is possible to create dinamyc styles but the logic to dinamyc styles is 
		more about if and props: NO LOOPS, no EXPORTABLE CONSTANTS, NO efects, NO 
		HOOKS,only styles, remember you have react and react components and 
		controllers to solve problems. YOU dont solve problems in the styles module.

	@Authors: Michel MS.
*/


import styled from '@emotion/styled';

export const DivHeader = styled.div`
	text-align: center;
	align-items: center;
`;

export const ImgHeader = styled.img`
	margin: 0 auto;
	width: 5%;
	border: 6px solid #00eb00;
	border-radius: 25px;
`;

