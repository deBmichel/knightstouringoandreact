import { useEffect } from 'react';

import { DivHeader, ImgHeader } from './styles.js';
import { H1AkTlvgl } from '../../common/styles/styles.js';
import { changeHeaderImg, ImageHeaderLogo } from './controller.js';

export const Header = () => {
	// I was reading about React and I found an idea to change the image in
	// https://reactjs.org/docs/hooks-effect.html
	useEffect(() => {
		changeHeaderImg();
		console.log("rendered Header");
	});

	return (<>
		<DivHeader>
			<H1AkTlvgl>The knight's tour</H1AkTlvgl>
			<ImgHeader 
				id="ktmi" alt="logo" src={ImageHeaderLogo} onClick={changeHeaderImg}
			/>
		</DivHeader>
	</>);
}
