import { css } from '@emotion/react';
import { createGlobalStyle } from 'styled-components';
import styled from '@emotion/styled';


import fontAkayaTelivigala from '../../../rsrcs/media/fonts/AkayaTelivigala-Regular.ttf';

// Interesting topic and I had new some ideas ....
// Global Styles are a great idea.
export const BaseFont = createGlobalStyle`
	@font-face {
		font-family: 'Akaya Telivigala';
		src: URL(${fontAkayaTelivigala}) format('truetype');
	}

	.BaseFont {
    	font-family: Akaya Telivigala, cursive;
    	color: #00eb00;
  	}
`

// Is @font-face convertible to function or const ??
export const H1AkTlvgl = styled.h1`
	@font-face {
		font-family: 'Akaya Telivigala';
		src: URL(${fontAkayaTelivigala}) format('truetype');
	}
	font-family: Akaya Telivigala, cursive;
    color: #00eb00;
`

export {fontAkayaTelivigala}

