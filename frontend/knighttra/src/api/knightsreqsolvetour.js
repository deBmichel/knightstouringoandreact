/*
	MODULE: knightsreqsolvetour.js

        The idea of this module is contains the logic about the requests related
        with the solution of a Knight's tour. Here I am using the next propagation
        schema from the api constants:

        	Minor explanation: RequestToSolveNewTour execute a post to solve a tour
        	sending a coodinate in the chess board to the api in the endpoint (Only
        	to solve the tour, To claim the tour is needed other endpoint and use
        	web sockets.): 
        		SOLVEAKNIGHTSTOUR_ENDPOINT = '/knighttra/knightstour/solveatour'. 
        	
        	The api can return the answers:

				REALLY_SOLVED_KNIGHTSTOUR = 'Tour Solved';
				TOUR_WAS_SOLVED = 'Tour was solved.'
				ERROR_BAD_COORDINATES_DATA = 'Problems with data in Coordinate: you are sending bad data'

			All this answers and endpoint have been defined in the module constants.js 
			but are using the related :

				// with the request to solve a knight's tour

			Here the imports are :

				SOLVEAKNIGHTSTOUR_ENDPOINT,
				REALLY_SOLVED_KNIGHTSTOUR,
				TOUR_WAS_SOLVED,
				ERRORSYSTEM,
				ERROR_BAD_COORDINATES_DATA

			But the exports are:
				
				REALLY_SOLVED_KNIGHTSTOUR,
				TOUR_WAS_SOLVED,
				ERRORSYSTEM,
				ERROR_BAD_COORDINATES_DATA

			As developer: why export the endpoint if the endpoint should be used only
			in this module ? Why React should use the endpoint ? React only needs answers
			react components should call the method or the http method handler. React 
			components should not why have code related to http requests methods a
			component only should call.

			AND ERRORSYSTEM ? What happens with this : simple error system is a bad joke,
			reason: An error system should be in a react context ; why ?:

				Context is designed to share data that can be considered “global” 
				for a tree of React components, such as the current authenticated 
				user, theme, or preferred language. For example, in the code below 
				we manually thread through a “theme” prop in order to style the 
				Button component:

				Taken from https://reactjs.org/docs/context.html

			One idea : REACT should not be about states REACT should be about communication.
			States for me are a bad idea , THE problem with states is big , the states 
			management is the cause of redux, I have to do the next question:

				why not use and propagate the use of statecharts ?
				why not use a Golang Communicating Sequential Processes style in react ?

    Sections:
		No sections are needed, this is a module designed to only one proposite :
		contains the logic about the requests related with the solution of a Knight's
		tour.
        
    @Authors: Michel MS.
*/

import { DoFetchGet, DoFetchPost } from './httpmethods.js';

import {
	SOLVEAKNIGHTSTOUR_ENDPOINT,
	REALLY_SOLVED_KNIGHTSTOUR,
	TOUR_WAS_SOLVED,
	ERRORSYSTEM,
	ERROR_BAD_COORDINATES_DATA
} from './constants.js';

export const RequestToSolveNewTour = (data) => {
	return DoFetchPost(SOLVEAKNIGHTSTOUR_ENDPOINT, data).then((jsonRegisterAnswer) => {
        if (jsonRegisterAnswer['Answer'] === REALLY_SOLVED_KNIGHTSTOUR) {
			return [true, jsonRegisterAnswer['idAnswer']];
		} else {
			if (jsonRegisterAnswer['Answer'] === ERROR_BAD_COORDINATES_DATA) {
				return [false, ERROR_BAD_COORDINATES_DATA];
			} 

			console.log("Really Found a diferent error", jsonRegisterAnswer);
			return [false, ERRORSYSTEM];
		}
	});
}

// I export here but I dont define here : why ? Simple It is about style. Review my idea 
// about propagation. ERRORSYSTEM ? Remember is a joke ...... review the context ...
export {
	SOLVEAKNIGHTSTOUR_ENDPOINT,
	REALLY_SOLVED_KNIGHTSTOUR,
	TOUR_WAS_SOLVED,
	ERROR_BAD_COORDINATES_DATA
}