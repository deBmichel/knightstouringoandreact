/*
	MODULE: knightsreqclaimtour.js

		The api use a web socket to claim the tour this module contains
		only one function for the objective : Claim the tour. The code to
		request SOLVE a tour is in the module MODULE: knightsreqsolvetour.js.

		Why use react to re-render the component designed to contain the status
		when jquery can change the real DOM in this proposite?

	@Authors: Michel MS.
*/
import $ from 'jquery'; 

import { API_WSCLAIMER } from './constants.js';

export const WebSocketHandlerToClaimSolvedTour = (idtour, finisher, listener) => {
	let wsckt = new WebSocket(API_WSCLAIMER+idtour);
	let messages = 0;

	wsckt.onmessage = (m) => {
		let msgstrdata = m.data;
		$("#changestatus").append($("<div></div><br />").text(m.data + " In Time " + new Date()));
		const coordinates = m.data.substring(m.data.indexOf("(")+1, m.data.indexOf(")")).split(",");
		const numericcrndts = coordinates.map( (coordinate) => parseInt(coordinate.split(":")[1]) );
		listener(numericcrndts, "Run knight's tour id: ", idtour);
		messages = messages + 1;
	}

	wsckt.onclose = () => {
		$("#changestatus").append($("<div></div>").text("<<< Really finished knight's tour"));
		let datefinish = new Date();
		finisher(datefinish+" with ("+messages+") received messages");
	}

	wsckt.onopen = () => {
		$('#changestatus').empty();
		$('#changestatus').html('>>> Running tour ...');
	}
}

