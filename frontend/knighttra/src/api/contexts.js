import { createContext } from 'react';

import {API_URL, FRONTEND_URL, ERRORSYSTEM} from './constants.js';

const GeneralApiContext = createContext({
	apiurl = API_URL,
	fronturl = FRONTEND_URL,
	errorsystem = ERRORSYSTEM
});
