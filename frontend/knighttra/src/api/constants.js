/*
    MODULE: constants.js

        The idea of this module is define the api constants, if you have
        used an api contract imagine this: This module save the answers 
        of the api for each call or status. The idea is to have one module
        to manage all api answers and status.

        Here I don't use a export section because all in this module should
        be exportable, is logic. The specific constants should be controlled 
        with a schema of specific PROPAGATION.

    Sections:
		
		No sections all in this module are constants related to the api behavior
		and answers. I preffer use simple related comments.
        
    @Authors: Michel MS.
*/


/*
	A great lesson for me I did not have to install dotenv, this is sure.
	react-scripts actually uses dotenv library under the hood.
	With react-scripts@0.2.3 and higher, you can work with environment 
	variables this way:

	1. create .env file in the root of the project
	2. set environment variables starting with REACT_APP_ there
	3. access it by process.env.REACT_APP_... in components

	Taken from : https://stackoverflow.com/questions/42182577/is-it-possible-to-use-dotenv-in-a-react-project
*/

// Constants related with the api access
export const API_URL=process.env.REACT_APP_API_URL;
export const API_WSCLAIMER=process.env.REACT_APP_WBSCKT_API_CLAIMER;
export const FRONTEND_URL=process.env.REACT_APP_FRONTEND_URL;
export const ERRORSYSTEM = "ERRORSYSTEM";

// Constants related with the request to solve a knight's tour
export const SOLVEAKNIGHTSTOUR_ENDPOINT = '/knighttra/knightstour/solveatour';
export const REALLY_SOLVED_KNIGHTSTOUR = 'Tour Solved';
export const TOUR_WAS_SOLVED = 'Tour was solved.'
export const ERROR_BAD_COORDINATES_DATA = 'Problems with data in Coordinate: you are sending bad data'

