/*
    MODULE: httpmethods.js

        The idea of this module is simple: HTTP methods should be generic
        I dont have why to have a lot of fetch in all the repo, I 
        preffer try to writte a simple rehusable code about http methods.

        My dream : Don't Repeat YourSelf, review at the same time the functions
        DoFetchPost DoFetchGet same idea (Call http method) different intention
        (Post(Data), Get(NO Data))

        I love arrow functions, they are funny for me.

    Sections:

        1: Section generic methods to operate PATCH, POST, GET, DELETE, etc...
            This section is a try to writte by myself a generic implementation
            of http methods.

        2: Exports
            This section specify what is exported from this module.

    @Authors: Michel MS.
*/

import {API_URL,FRONTEND_URL} from './constants.js';

//Begin : Section generic methods to operate PATCH, POST, GET, DELETE, etc...

const baseFetchHeaders = (Method) => {
    return {
        method: Method,
        headers: {
            'Access-Control-Allow-Origin': FRONTEND_URL
        },
    };
};

const ExecuteSimpleFetch = async (endpointUrl, method, data) => {
    let url = API_URL + endpointUrl;
    var fetchHeaders = baseFetchHeaders(method);
    if (data !== undefined) {
        console.log("setting data", data);
        fetchHeaders['body'] = data;    
    }
    
    let answerpost = undefined;
    const postPromise = await fetch(url, fetchHeaders);
    try {
        answerpost = await postPromise.json();
        console.log("Arrived", answerpost);
    } catch(e) {
        answerpost = e;
        console.log('error:', e, e.message);
    }        
    return answerpost;
}

const DoFetchPost = async (endpointUrl, data) => {
    return await ExecuteSimpleFetch(endpointUrl, 'POST', data); 
}

const DoFetchGet = async (endpointUrl) => {
    return await ExecuteSimpleFetch(endpointUrl, 'GET'); 
}

//End : Section generic methods to operate PATCH, POST, GET, DELETE



//Begin : Exports

export {DoFetchPost, DoFetchGet}

//End : Exports
