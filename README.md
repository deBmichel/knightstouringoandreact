<div align="center">
	<h1>The Knights Tour</h1>
	<img src="reporsrcs/Knights-Tour-Animation.gif" alt="Knightgif" width="126px" height="126px"/>
	<img src="reporsrcs/logo.png" alt="Knightlogo"/>
	<img src="reporsrcs/Knights-Tour-Animation.gif" alt="Knightgif" width="126px" height="126px"/>

	- It is a solution of the Knights Tour problem using golang and react.
</div>


It is a personal software development project that aims to make a study about euristics 
techniques, Golang and React in software development. This project uses an approach to 
Warnsdorff's rule (Warnsdorff's rule is a heuristic for finding a single knight's tour.) 
to solve The knight's tour problem (The knight's tour problem is the mathematical problem
of finding a knight's tour.)

you can find some information about knight's tour at:

<div align="center">
	<h2>https://en.wikipedia.org/wiki/Knight's_tour</h2>
</div>

Aditionally shoud find a functional deploy of this project at:

<div align="center">
	<h2>
		Fronted : https://frontknightstour.herokuapp.com/
	</h2>
	
</div>

<div align="center">
	<h2>Backend : https://knightstourbackend.herokuapp.com/knighttra/knightstour/solvedtours</h2>
</div>

